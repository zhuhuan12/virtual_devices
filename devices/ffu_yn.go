package devices

import (
	"context"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/things-go/go-modbus"
)

// FFU雅宁

type FfuYN struct {
	Product      string // 产品Key
	ModbusAddr   int8   // modbus地址
	SwitchStatus int    // 开关状态
	Speed        int    // 风速
}

func (f *FfuYN) NewDevice(modbusValue int8) Device {
	return &FfuYN{
		ModbusAddr:   modbusValue,
		Product:      "ffu_yn",
		SwitchStatus: 1,
		Speed:        1,
	}
}

func (f *FfuYN) GetProductKey() string {
	return f.Product
}

func (f *FfuYN) GetModbusAddr() int8 {
	return f.ModbusAddr
}

func (f *FfuYN) HandleData(data []byte) (out []byte, err error) {
	out = []byte{}
	if data[1] == 0x03 {

		beginAddr := int(data[2])<<8 | int(data[3]) // 起始地址
		readLen := int(data[4])<<8 | int(data[5])   // 读取长度
		g.Log().Debug(context.Background(), "ffu_yn", g.Map{"beginAddr": beginAddr, "readLen": readLen})
		out = []byte{byte(f.ModbusAddr), 0x03, byte(readLen * 2)}
		for i := 0; i < readLen; i++ {
			addr := beginAddr + i // 对应的地址
			switch addr {
			case 0x00:
				out = append(out, 0, byte(f.SwitchStatus)) // 开关状态
			case 0x03:
				out = append(out, byte(f.Speed>>8), byte(f.Speed)) // 风速
			default:
				out = append(out, 0, 0)
			}
		}
	} else if data[1] == 0x06 {
		addr := int(data[2])<<8 | int(data[3])
		value := int(data[4])<<8 | int(data[5])
		g.Log().Debug(context.Background(), "控制", g.Map{"addr": addr, "value": value})
		switch addr {
		case 0:
			f.SwitchStatus = value
		case 3:
			f.Speed = value
		}
		out = []byte{byte(f.ModbusAddr), 0x06, byte(addr >> 8), byte(addr), data[4], data[5]}
	}
	intValue := modbus.CRC16(out)
	out = append(out, byte(intValue), byte(intValue>>8))
	return
}
