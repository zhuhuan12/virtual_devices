package devices

import (
	"context"
	"encoding/hex"
	"testing"

	"github.com/gogf/gf/v2/frame/g"
)

func TestData(t *testing.T) {
	out := []byte{0x01, 0x03, 0x00, 0x00, 0x00, 0x06, 0xc5, 0xC8}
	devices = append(devices, &WyzParticle{
		ModbusAddr: 1,
		RunMin:     2,
		RunMode:    3,
		Um03Yz:     4})
	out, err := DataIn(out)
	if err != nil {
		t.Fatal(err)
		return
	}
	g.Log().Debug(context.Background(), hex.EncodeToString(out))
	t.Fail()
}
