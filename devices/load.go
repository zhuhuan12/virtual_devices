package devices

import (
	"context"
	"fmt"
	"strings"
	"sync"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gconv"
)

var products = make(map[string]Device, 0)
var lock sync.RWMutex
var devices = make([]Device, 0)

// 先初始化再加载
// 加载设备
func Load() {
	lock.Lock()
	defer lock.Unlock()
	for product, deviceFactory := range products {
		configKey := fmt.Sprintf("devices.%s", product)
		val := g.Config().MustGet(context.TODO(), configKey, "").String()
		if val == "" {
			continue
		}
		modbus := strings.Split(val, ",")
		for _, mod := range modbus {
			modV := gconv.Int8(mod)
			if modV == 0 {
				continue
			}
			device := deviceFactory.NewDevice(modV)
			devices = append(devices, device)
		}
	}
}


