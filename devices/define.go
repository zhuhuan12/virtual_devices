package devices

// 定义虚拟设备
type Device interface {
	NewDevice(modbusValue int8) Device
	GetProductKey() string                  // 获取产品key
	GetModbusAddr() int8                    // 获取modbus地址
	HandleData(data []byte) ([]byte, error) // 处理数据
}
