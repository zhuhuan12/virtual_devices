package devices

import (
	"context"
	"encoding/hex"
	"fmt"

	"github.com/gogf/gf/v2/frame/g"
	"github.com/things-go/go-modbus"
)

// 设备通讯数据的处理
// 对外的服务

func DataIn(data []byte) ([]byte, error) {
	lock.Lock()
	defer lock.Unlock()
	if len(data) <= 4 {
		return nil, fmt.Errorf("数据长度不够")
	}
	getCrc16 := uint16(data[len(data)-1])<<8 | uint16(data[len(data)-2])
	intValue := modbus.CRC16(data[:len(data)-2])
	if intValue != getCrc16 {
		g.Log().Warning(context.Background(), "crc校验失败", g.Map{"data": hex.EncodeToString(data), "crc": intValue, "getCrc16": getCrc16})
		return nil, fmt.Errorf("crc校验失败")
	}
	for _, device := range devices {
		if device.GetModbusAddr() == int8(data[0]) {
			g.Log().Debug(context.Background(), "检测到设备类型", device.GetProductKey())
			return device.HandleData(data)
		}
	}
	return nil, fmt.Errorf("未找到设备")
}
