package devices

import "github.com/things-go/go-modbus"

// 灯光 的控制
type Light struct {
	Product    string // 产品Key
	ModbusAddr int8   // modbus地址
	Switch     int    // 开关状态
}

func (l *Light) NewDevice(modbusValue int8) Device {
	return &Light{
		ModbusAddr: modbusValue,
		Product:    "light",
		Switch:     1,
	}
}

func (l *Light) GetProductKey() string {
	return l.Product
}

func (l *Light) GetModbusAddr() int8 {
	return l.ModbusAddr
}

func (l *Light) HandleData(data []byte) (out []byte, err error) {
	out = []byte{}
	if data[1] == 0x01 { // 读取
		beginAddr := int(data[2])<<8 | int(data[3]) // 起始地址
		readLen := int(data[4])<<8 | int(data[5])   // 读取长度
		out = []byte{byte(l.ModbusAddr), 0x01, byte(readLen)}
		for i := 0; i < readLen; i++ {
			addr := beginAddr + i // 对应的地址
			switch addr {
			case 0:
				out = append(out, 0, byte(l.Switch)) // 开关状态
			}
		}
	} else if data[1] == 0x05 { // 写入
		addr := int(data[2])<<8 | int(data[3])
		value := int(data[4])<<8 | int(data[5])
		switch addr {
		case 0:
			if value > 0 {
				l.Switch = 1
			} else {
				l.Switch = 0
			}
		}
		out = []byte{byte(l.ModbusAddr), 0x05, byte(addr >> 8), byte(addr), data[4], data[5]}
	}
	intValue := modbus.CRC16(out)
	out = append(out, byte(intValue), byte(intValue>>8))
	return
}
