package devices

func init() {
	products = map[string]Device{
		"ffu_zx":   &FfuZx{},
		"ffu_yn":   &FfuYN{},
		"light":    &Light{},
		"lizishu":  &WyzParticle{},
		"pressure": &Pressure{},
		"fengsu":   &FengSu{},
	}
	Load()
}
