package main

import (
	"context"
	"encoding/hex"
	"sync"
	"time"
	"virtual_device/devices"

	"github.com/gogf/gf/v2/frame/g"
	"go.bug.st/serial"
)

func main() {
	uart := "/COM32"
	mode := &serial.Mode{
		BaudRate: 9600,
	}
	// 串口初始化
	port, err := serial.Open(uart, mode)
	if err != nil {
		panic(err)
	}
	defer port.Close()
	go msgListen(port)
	select {}
}

func msgListen(port serial.Port) {
	noDataTime := 170 * time.Millisecond
	buff := make([]byte, 1000)
	allBuff := make([]byte, 0)
	noData := time.NewTimer(noDataTime) //一次性触发定时器；空白间隔
	var dataLock sync.Mutex

	go func() {
		for {
			n, err := port.Read(buff)
			noData.Reset(noDataTime)
			if err != nil || n == 0 {
				return
			}
			dataLock.Lock()
			allBuff = append(allBuff, buff[:n]...)
			dataLock.Unlock()
		}
	}()

	for { //注意这里不能遵守编辑器建议
		select {
		case <-noData.C:
			if len(allBuff) == 0 {
				continue
			}
			dataLock.Lock()
			getBuf := allBuff
			allBuff = []byte{}
			dataLock.Unlock()

			g.Log().Debug(context.Background(), "收到usb口的数据v1.2", hex.EncodeToString(getBuf))
			backdata, err := devices.DataIn(getBuf)
			if err != nil {
				g.Log().Warning(context.Background(), "数据处理失败", err)
				continue
			}
			if len(backdata) == 0 {
				continue
			} else {
				g.Log().Debug(context.Background(), "返回数据", hex.EncodeToString(backdata))
			}
			_, err = port.Write(backdata)
			if err != nil {
				g.Log().Warning(context.Background(), "usb口返回数据失败", err)
			}
		}
	}
}
